terraform {
  backend "s3" {}
}

provider "aws" {
  region = var.region
}

output "hello_world" {
  value = "Hello, World!"
}
