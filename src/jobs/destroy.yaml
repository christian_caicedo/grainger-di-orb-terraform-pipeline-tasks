# src/jobs/destroy.yaml

description: Run terraform destroy for << parameters.env >>

executor: << parameters.executor >>

parameters:
  account:
    description: s3 bucket name
    type: string
  after-terraform:
    description: Optional steps to run after running terraform destroy.
    type: steps
    default: []
  aws-region:
    description: aws region provisioned and tested.  Must also be set in "provider.region" in your "variables.tf"
    type: string
  before-terraform:
    description: Optional steps to run before running terraform destroy.
    type: steps
    default: [ ]
  bucket-region:
    description: aws region of terrafrom state s3 bucket
    type: string
  circle-ci-service-account:
    description: Please look in the global context for your team's parameter
    type: string
  env:
    description: infrastructure environment name
    type: string
  var-prefix:
    description: Prefix for tfvars and json parameters file. If this is defined, this would be used as the prefix for json and tfvars files.
    type: string
    default: ""
  bucket-name:
    description: terrafrom state s3 bucket name. This would override, bucket-name coined using team-name and account
    type: string
    default: ""
  bucket-key:
    description: terrafrom state s3 bucket key. This would override, bucket-key coined using pipeline and env
    type: string
    default: ""
  executor:
    description: Name of custom executor to use
    type: executor
    default: default
  extra-params:
    description: Any extra params to be passed to terraform destroy.
    type: string
    default: ""
  integration-tests:
    description: test aws configuration
    type: steps
    default: [ ]
  log-level:
    description: TF_LOG setting for debugging
    type: enum
    default: ""
    enum: [ TRACE, DEBUG, INFO, WARN, ERROR, "" ]
  pipeline:
    description: by convention, state info for an infrastructure pipeline is named as the repo containing the pipeline code
    type: string
    default: $CIRCLE_PROJECT_REPONAME
  statefile:
    description: by convention, state files are named terraform
    type: string
    default: terraform
  team-name:
    description: team managing the infrastructure. Used as prefix of S3 bucket that contains the statefile.
    type: string
  vault-addr:
    description: Override VAULT_ADDR coming from context.
    type: string
    default: $VAULT_ADDR
  working-directory:
    description: specify a specific folder in which to run the commands
    type: string
    default: "~/project"
  set-git-ssh-command:
    description: set git ssh env variables
    type: boolean
    default: true
  executor-type:
    description: the executor type for running terraform [ DOCKER, RUNNER ]
    type: enum
    default: "DOCKER"
    enum: [ DOCKER, RUNNER ]

steps:
  - checkout
  - when:
      name: setup_remote_docker
      condition:
        equal: [ << parameters.executor-type >>, DOCKER ]
      steps:
        - setup_remote_docker
  - prepare-terraform:
      team-name: << parameters.team-name >>
      account: << parameters.account >>
      pipeline: << parameters.pipeline >>
      env: << parameters.env >>
      bucket-name: << parameters.bucket-name >>
      bucket-key: << parameters.bucket-key >>
      statefile: << parameters.statefile >>
      circle-ci-service-account: << parameters.circle-ci-service-account >>
      vault-addr: << parameters.vault-addr >>
      aws-region: << parameters.aws-region >>
      bucket-region: << parameters.bucket-region >>
      working-directory: << parameters.working-directory >>
      log-level: << parameters.log-level >>
      before-terraform: << parameters.before-terraform >>
      set-git-ssh-command: << parameters.set-git-ssh-command >>
  - run:
      name: terraform destroy
      working_directory: << parameters.working-directory >>
      command: |
        if [ -z "<< parameters.var-prefix >>" ]; then
          VAR_FILE=<< parameters.env >>.json
          if [ ! -f "./$VAR_FILE" ]; then
            VAR_FILE=<< parameters.env >>.tfvars
          fi
        else
          VAR_FILE=<< parameters.var-prefix >>.json
          if [ ! -f "./$VAR_FILE" ]; then
            VAR_FILE=<< parameters.var-prefix >>.tfvars
          fi
        fi
        if [ ! -z "<< parameters.log-level >>" ]; then
          export TF_LOG=<< parameters.log-level >>
        fi
        terraform version
        terraform destroy -var-file="$VAR_FILE" -auto-approve
  - when:
      name: Run after-terraform lifecycle hook steps
      condition: << parameters.after-terraform >>
      steps: << parameters.after-terraform >>
