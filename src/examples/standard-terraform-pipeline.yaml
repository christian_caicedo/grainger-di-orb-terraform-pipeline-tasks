description: |
  Set of jobs that support the standard terraform pipeline workflow in an aws context. Assumes use of either
  awspec or inspec for aws configuration testing (default is awspec).
  NOTE: Parameter bucket-region is to indicate the aws region of the terraform state s3 bucket.

usage:
  version: 2.1
  orbs:
    terraform: wwgrainger/terraform-pipeline-tasks@7
  workflows:
    terraform-pipeline:
      jobs:
        - terraform/lint
        - terraform/plan:
            name: dev-release-plan
            team-name: "mobile-bff"
            account: "nonprod"
            env: "dev"
            var-prefix: "Optional - If defined, this would be used as the prefix for json and tfvars files"
            bucket-name: "terrafrom state s3 bucket name. Optional - This would override, bucket-name coined using team-name and account"
            bucket-key: "terrafrom state s3 bucket key. Optional - This would override, bucket-key coined using pipeline and env"
            aws-region: us-east-1
            bucket-region: us-east-2
            circle-ci-service-account: $SERVICE_USER_DI
            filters:
              branches:
                only: master
              tags:
                ignore: /.*/
        - approve-dev-release-plan:
            type: approval
            requires:
              - dev-release-plan
        - terraform/apply:
            name: apply-dev-release-plan
            team-name: "mobile-bff"
            account: "nonprod"
            env: "dev"
            var-prefix: "Optional - If defined, this would be used as the prefix for json and tfvars files"
            bucket-name: "terraform state s3 bucket name. Optional - This would override, bucket-name coined using team-name and account"
            bucket-key: "terraform state s3 bucket key. Optional - This would override, bucket-key coined using pipeline and env"
            aws-region: us-east-1
            bucket-region: us-east-2
            circle-ci-service-account: $SERVICE_USER_DI
            extra-params: "-var 'input_var=input_value'"
            integration-tests:
              - run:
                  name: awspec tests
                  environment:
                    PLATFORM_ENV: dev
                  command: rspec ../integration/default
            requires:
              - approve-dev-release-plan
        # release candidate
        - terraform/plan:
            name: qa-release-plan
            team-name: "mobile-bff"
            account: "nonprod"
            env: "qa"
            var-prefix: "Optional - If defined, this would be used as the prefix for json and tfvars files"
            bucket-name: "terrafrom state s3 bucket name. Optional - This would override, bucket-name coined using team-name and account"
            bucket-key: "terrafrom state s3 bucket key. Optional - This would override, bucket-key coined using pipeline and env"
            aws-region: us-east-1
            bucket-region: us-east-2
            circle-ci-service-account: $SERVICE_USER_DI
            filters:
              branches:
                ignore: /.*/
              tags:
                only: /.*/
        - approve-qa-release-plan:
            type: approval
            requires:
              - qa-release-plan
            filters:
              branches:
                ignore: /.*/
              tags:
                only: /.*/
        - terraform/apply:
            name: apply-qa-release-plan
            team-name: "mobile-bff"
            account: "nonprod"
            env: "qa"
            var-prefix: "Optional - If defined, this would be used as the prefix for json and tfvars files"
            bucket-name: "terraform state s3 bucket name. Optional - This would override, bucket-name coined using team-name and account"
            bucket-key: "terraform state s3 bucket key. Optional - This would override, bucket-key coined using pipeline and env"
            aws-region: us-east-1
            bucket-region: us-east-2
            integration-tests:
              - run:
                  name: awspec tests
                  environment:
                    PLATFORM_ENV: qa
                  command: rspec ../integration/default
            requires:
              - approve-qa-release-plan
        - terraform/plan:
            name: prod-release-plan
            team-name: "mobile-bff"
            account: "prod"
            env: "prod"
            var-prefix: "Optional - If defined, this would be used as the prefix for json and tfvars files"
            bucket-name: "terraform state s3 bucket name. Optional - This would override, bucket-name coined using team-name and account"
            bucket-key: "terraform state s3 bucket key. Optional - This would override, bucket-key coined using pipeline and env"
            aws-region: us-east-1
            bucket-region: us-east-2
            filters:
              branches:
                ignore: /.*/
              tags:
                only: /.*/
        - approve-prod-release-plan:
            type: approval
            requires:
              - prod-release-plan
            filters:
              branches:
                ignore: /.*/
              tags:
                only: /.*/
        - terraform/apply:
            name: apply-prod-release-plan
            team-name: "mobile-bff"
            account: "prod"
            env: "prod"
            var-prefix: "Optional - If defined, this would be used as the prefix for json and tfvars files"
            bucket-name: "terraform state s3 bucket name. Optional - This would override, bucket-name coined using team-name and account"
            bucket-key: "terraform state s3 bucket key. Optional - This would override, bucket-key coined using pipeline and env"
            aws-region: us-east-1
            bucket-region: us-east-2
            circle-ci-service-account: $SERVICE_USER_DI
            integration-tests:
              - run:
                  name: awspec tests
                  environment:
                    PLATFORM_ENV: prod
                  command: rspec ../integration/default
            requires:
              - approve-prod-release-plan
        - terraform/destroy:
            name: apply-prod-release-plan
            team-name: "mobile-bff"
            account: "prod"
            env: "prod"
            var-prefix: "Optional - If defined, this would be used as the prefix for json and tfvars files"
            bucket-name: "terraform state s3 bucket name. Optional - This would override, bucket-name coined using team-name and account"
            bucket-key: "terraform state s3 bucket key. Optional - This would override, bucket-key coined using pipeline and env"
            aws-region: us-east-1
            bucket-region: us-east-2
            circle-ci-service-account: $SERVICE_USER_DI
            requires:
              - terraform/apply
