#Updating to newer version of the orb:

###Major version upgrade requirements (2.X -> 3.0):
[BitBucket could not go further back than version 3.0]

###Minor version upgrade:
-New feature: Toggle for setup-job.yaml decrypt (3.1)
-Modified sts-vault-path (the path no longer appends provision when looking at credentials) (3.1)
-Added lint and validate job to branch pipeline (3.4)
