#Changelog

### Moving away from env.ci

**In the next major version we hope to remove decrypt-env**

These changes apply the new authentication tooling that what changed in the common-pipeline-tasks. We are trying to move away a model of having an encrypted file in your repo to secrets stored in vault. And we are looking to Vault to be the manager of temporary tokens rather then calling aws sts directly. Everything you get access to should be through temporary tokens (provision by vault). To do this we have made changes to the interfaces of `login-to-vault`, `setup-aws-credenitals` which are called by `prepare-terraform`

With this change you will need to make sure that you have a service user token (vault token) in the circleci context to reference in your build. This service user will have the same team-level permissions that you have when you log into vault. Once you log into vault, you can provision temporary aws tokens or get other shared secrets (like datadog access tokens), or team secrets.

The changes to these interfaces really has you deleting references to variables that were being provided by circleci and replacing it with a reference to the service account and path in vault

> See an example here: https://bitbucket.org/wwgrainger/platformtest-dogfood

###Major version upgrade (2.X -> 3.0):
[BitBucket could not go further back than version 3.0]

##[Changed from 3.0.0 -> 4.0.0]
 - Split orb out into its own repo (3.0)
 - Upgraded @orb.yaml dependencies (3.1)
 - Added toggle for setup-job.yaml decrypt (3.1)
 - Added .yamllint and task to invoke validate (3.1)
 - Moved toggle from 'string' to 'boolean' (3.1)
 - Upgraded orb-tools to 1.5.0 & removed access keys from env.ci (3.1)
 - Updated examples (3.1)
 - Modified sts-vault-path (3.1)
 - Updated common pipeline tasks (3.2)
 - Updated orb-tools version (3.2)
 - Fixed padding for argument in setup-job command (3.2)
 - Corrected BASH_ENV so it's user agnostic (3.3)
 - Added throughput metrics (3.3)
 - Added lint and validate job to branch pipeline (3.4)
 - Added task to publish dev orb version (3.4)
 - Updated metrics orb to 1.3.0 (3.4)
 - Added contract tests to pipeline (3.4)
 - Used latest prod version of orb-tools (3.4)
 - Pinned orb-tools to major version only (3.4)
 - Updated orb-tools to 4.0.1 (3.4)
 - Installed common dev tasks (3.4)
 - Updated with latest base agent version (3.4)
 - Pinned base agent to major version (3.4)
 - Used absolute path for bash env [3.4.1]
 - Report terraform version whenever running terraform [3.4.2]
 - Removed di-demo
 - Added throughout metrics
 - Updated executor to base-agent [3.4.3]
 - Updated README
 - Added lint and validate job to branch pipeline
 - Updated README.md to include orb version info
 - Added task to publish dev orb version
 - Updated metrics orb to 1.3.0
 - Updated orb-tools to 4.0.1
 - Installed common dev tasks
 - Updated with latest base agent version
 - Pinned base agent to major version
 - Edited CHANGELOG and added UPGRADE.md
 - Added special note concerning 2.X changes
 - updated executor to pull from ECR prod, not Quay. Added pre-commit-config, updated metrics orb version
 - Retooled pipeline to leverage more standard 2 workflows, prod workflow triggered from tags.
 - Updated orb-tools to version 5 [4.0.0]
   
##[Changed from 3.0.0 -> 4.0.0]
 - Updated orbs and executors 
 - Used ECR global-context variables for Executor definition [4.1.0]
 - Updated Terraform jobs/command/executor and common-orb [5.0.0]

##[Changed from 5.0.0 -> 6.0.0]
 - Updated common pipeline task orb in example
 - Updated examples on orb usage

##[Changed from 6.0.0 -> 6.1.0]
-Addition of var-prefix parameter to apply/destroy/plan/plan-destroy jobs. Also logic to handle if var-prefix parameter exists.

##[Changed from 6.1.0 -> 6.2.0]
-Addition of bucket-name and bucket-key parameter to apply/destroy/plan/plan-destroy jobs and prepare-terraform command. Also logic to handle if var-prefix parameter exists.
