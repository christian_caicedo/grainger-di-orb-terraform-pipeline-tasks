"""local invoke scripts"""
import json

from grainger.di.ci.dev_tasks import tasks as dev_tasks
from grainger.di.ci.orb_tasks import tasks as orb_tasks
from invoke import task, Collection
from invoke.tasks import call

ns = Collection()


def get_inputs_as_json():
    with open('inputs.json') as json_file:
        data = json.load(json_file)
    if data['orb_name'] == '':
        print('Please update ./inputs.json with your orb information and try again!')
        exit()
    else:
        return data


ORB_NAME = get_inputs_as_json()['orb_name']
dev_tasks.ORB_NAME = ORB_NAME


@task(call(orb_tasks.lint,
           custom_rules_filepath="",
           lint_dir=".",
           use_default_rules=True))
def lint(ctx):
    """Lint an orb"""
    pass


@task(call(orb_tasks.validate,
           destination_orb_path="orb.yml",
           source_dir="src"))
def validate(ctx):
    """Validate an orb"""
    ctx.run(f'orb-tasks validate --source-dir src --destination-orb-path orb.yml')


@task(help={'tag': 'Git tag that represents the version being released'})
def contract_tests(ctx, tag):
    """Contract test an orb"""
    ctx.run(f'orb-tasks contract-tests --orb-name "wwgrainger/{ORB_NAME}" --source-dir src --git-tag "{tag}"')


@task(call(orb_tasks.release_dev,
           destination_orb_path="orb.yml",
           source_dir="src",
           orb_name=f"wwgrainger/{ORB_NAME}",
           commit_id="latest"))
def publish_dev_orb(ctx):
    """Publish dev:latest version of an orb"""
    pass


@task(help={'tag': 'Git tag that represents the version being released'})
def publish_prod_orb(ctx, tag):
    """Publish the dev:latest version of the orb to production with tag"""
    ctx.run(f'orb-tasks release-prod '
            f'--packed-orb-path "orb.yml" '
            f'--orb-name "wwgrainger/{ORB_NAME}" '
            f'--commit-id "latest"'
            f' --git-tag "{tag}"')


ns.add_task(lint)
ns.add_task(validate)
ns.add_task(contract_tests)
ns.add_task(publish_dev_orb)
ns.add_task(publish_prod_orb)
ns.add_task(dev_tasks.precommit)
ns.add_task(dev_tasks.pip_login)
