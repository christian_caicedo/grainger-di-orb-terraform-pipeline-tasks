## Conventions

#### branching


Local development should be done on a feature branch that will be merged into master upon approval of a merge request. 

### source layout

The standard circleci orb source layout is this:

```
src/
├── @orb.yaml
├── commands
│   ├── a.yaml
│   └── b.yaml
├── examples
│   └── c.yaml
├── executors
│   └── d.yaml
└── jobs
    ├── x.yaml
    ├── y.yaml
    └── z.yaml
```

### source packaging

To see how the code in the separate files is combined and run by circleci, there are choices:

1. `$ circleci config pack src > orb.yml`
1. 'Orb Source' at
   https://circleci.com/orbs/registry/orb/wwgrainger/terraform-pipeline-tasks
1. in the circleci ui for a given job in a given workflow, the `Configuration` tab will include the orb source along with the project's `.circleci/config.yml` and any other orbs

### semantic version promotion

1. `patch` - e.g. 0.0.1 -> 0.0.2 - default, but only if there are changes under `src` ( based on diff with current published version at https://circleci.com/orbs/registry/orb/wwgrainger/our-orb ).  If you just change the `README` or `config.yml`, there will be no version bump even if you click `approve-release` in circleci.
1. `minor` - e.g. 0.0.2 -> 0.1.0 - add any file called `src/minor`, and the version will be bumped whether there are changes under `src` or not.
1. `major` - e.g. 0.1.0 -> 1.0.0 - add any file called `src/major`, and the version will be bumped whether there are changes under `src` or not.  If there's also `src/minor`, major will take precedence.

### git repo tagging

When the version is promoted, a tag is pushed to the git repo, e.g. `v1.2.23`

This requires write access to the git repo, enabled by adding group `Delivery Infrastructure Tagging` to the repo in Bitbucket.

### orb versions

#### development

Development orbs are mutable and expire after 90 days. They are tagged with the format: `dev:<< string >>`. 

The `orb-tools/publish-dev` job in the pipeline will publish the latest code changes to version `@dev:latest` and `@dev:${CIRCLE_SHA1:0:7}` of the orb. The CIRCLE_SHA1 version is outputted in the pipeline in case you want to use a later version. 

You can also publish a dev version of the orb from your local machine by doing the following: 
1. Log into vault
2. Run task: `invoke publish-dev-orb --version-label=<string>`

To test with the dev version of an orb, simply define the dev version of that orb in your circleci config. 

#### production

Production orbs are immutable and do not expire. They are published using the semantic versioning scheme.

Each time the job `orb-tools/promote-semver` is run in the pipeline, the latest dev version of the orb is promoted to production.  

All information on published versions of the orb are documented here: https://circleci.com/orbs/registry/orb/wwgrainger/terraform-pipeline-tasks.

### testing

#### local

For contract tests:
1. Log into docker using quay credentials
2. Run `invoke contract-tests`

#### pipeline
No pipeline tests currently exist.

### Local Tasks

Python `invoke` is used to run local developement and test tasks.  Some tasks are installed from common grainger task libs at `delivery-platform-pypi`.

#### Setup

Create a python virtual env for this repo's tasks:

```shell
  $ python3 -m venv .venv && source .venv/bin/activate && pip install -U pip
```

To be able to pip install grainger packages, such as those in `requirements.txt`, add the following to your `~/.config/pip/pip.conf`:

```shell
[global]
extra-index-url = https://nexus.grainger.com:8443/nexus/repository/delivery-platform-pypi/simple/
trusted-host = nexus.grainger.com

```

Then, you'll need VPN access for pip to access that `extra-index-url`.

```shell
  $ pip install -r requirements.txt
```

#### orb schema validation

run `invoke dev.validate`

#### contract tests

1. Log into vault
1. Log into docker using quay credentials
1. Run `invoke dev.contract-tests`

